# kub-work-dois-by-orcid



## Scope

Web component, based upon Lit, for getting a list of work [DOI](https://doi.org/)s for an [ORCID](https://orcid.org/) account. This is done by using the [ORCID's public APIs](https://info.orcid.org/documentation/features/public-api/) and [OpenCitations public APIs](https://opencitations.net/index/api/v1).

## Description

Attributes: @orcid, @period.
