import { LitElement, html, css } from "https://cdn.jsdelivr.net/npm/lit/+esm";

const styles =
    css`
        div#orcid-container {
            display: flex;
            justify-content: space-between;
        }
        
        sl-input#orcid-id {
            width: 30vw;
        }
        
        div#works-container {
            display: flex;
            flex-direction: column;
        }

        div.work {
            padding: 5px;
            display: flex;
            flex-direction: column;            
        }

        div.work:nth-child(even) {
            background: #fffff;
        }

        div.work:nth-child(odd) {
            background: #cccccc;
        }
        
        div.work-title {
            text-align: justify;
        }
`;

const properties = {
    person_name: {
        type: String,
    },
    works: {
        type: Array,
    },
};

const get_citations = external_ids => {
    let citations = [];

    external_ids.forEach(async external_id => {
        let type = external_id.type;
        let value = external_id.value;

        switch (type) {
            case "doi":
                let open_citations_citations_response = await fetch(new URL(`https://opencitations.net/index/api/v1/citation-count/${value}`))
                    .then(response => response.json());

                console.log(open_citations_citations_response);

                if (open_citations_citations_response.length > 0) {
                    citations.push(open_citations_citations_response[0].count);

                    return `${citations[0]} citări`;
                } else {
                    return `0 citări`;
                }

                break;
        }
    });


};
// /<sl-spinner></sl-spinner>
export default class WorkDoisByOrcid extends LitElement {
    static styles = styles;
    static properties = properties;

    constructor() {
        super();

        this.works = [];
    }

    render() {
        return html`
        <div id="orcid-container">
            <sl-input id="orcid-id" clearable placeholder="Identificator ORCID"></sl-input>
            <sl-button id="get-works" variant="primary">Căutare</sl-button>
        </div>
        <h4>Lucrări pentru <output>${this.person_name}</output></h4>
        <div id="works-container">
            ${this.works.map(work => {

            return html`
                    <div class="work">
                        <div class="work-title">${work.title}</div>
                        <div>
                            (<i>${work.type}</i>)
                            <output>${work.citations} citări după DOI</output>
                        </div>
                    </div>
                `
        })}        
        </div>        
`;
    }

    createRenderRoot() {
        const root = super.createRenderRoot();

        root.addEventListener("click", async event => {
            const target = event.target;

            if (target.matches("sl-button#get-works")) {
                let orcid_id = root.querySelector("sl-input#orcid-id").value;


                let orcid_record_url = get_orcid_record_url(orcid_id);
                let orcid_record = await fetch(orcid_record_url)
                    .then(response => response.text());
                //let orcid_record = xml;

                // parse the record
                const parser = new DOMParser();
                const orcid_doc = parser.parseFromString(orcid_record, "application/xml");

                let person_name = Array.from(orcid_doc
                    .querySelectorAll("given-names, family-name"))
                    .map(item => item.textContent)
                    .join(" ");
                this.person_name = person_name;

                let work_elements = orcid_doc.querySelectorAll("works > group");
                let works = [];
                
                for (const work_element of work_elements) {
                    let citations_array = [0];
                    let title = work_element.querySelector("work-summary > title > title").textContent;
                    let type = work_element.querySelector("work-summary > type").textContent;

                    // get the citations
                    let external_id_elements = work_element.querySelectorAll(":scope > external-ids > external-id");
                    for (const external_id_element of external_id_elements) {
                        let type = external_id_element.querySelector("external-id-type").textContent;
                        let value = external_id_element.querySelector("external-id-value").textContent;

                        switch (type) {
                            case "doi":
                                let open_citations_citations_response = await fetch(new URL(`https://opencitations.net/index/api/v1/citation-count/${value}`))
                                    .then(response => response.json());

                                if (open_citations_citations_response.length > 0) {
                                    citations_array[0] = open_citations_citations_response[0].count;
                                }

                                break;
                        }
                    }

                    let citations = citations_array[0];
                    console.log(citations_array);
                    works.push({
                        title,
                        type,
                        citations,
                    });
                }

                this.works = works;
            }
        });

        return root;
    }
}

const get_orcid_record_url = orcid_id => {
    return new URL(`https://pub.orcid.org/v3.0/${orcid_id}/record`);
};

window.customElements.define("kub-work-dois-by-orcid", WorkDoisByOrcid);


let xml =
    `
<record:record path="/0000-0002-9880-5215" xmlns:internal="http://www.orcid.org/ns/internal" xmlns:education="http://www.orcid.org/ns/education" xmlns:distinction="http://www.orcid.org/ns/distinction" xmlns:deprecated="http://www.orcid.org/ns/deprecated" xmlns:other-name="http://www.orcid.org/ns/other-name" xmlns:membership="http://www.orcid.org/ns/membership" xmlns:error="http://www.orcid.org/ns/error" xmlns:common="http://www.orcid.org/ns/common" xmlns:record="http://www.orcid.org/ns/record" xmlns:personal-details="http://www.orcid.org/ns/personal-details" xmlns:keyword="http://www.orcid.org/ns/keyword" xmlns:email="http://www.orcid.org/ns/email" xmlns:external-identifier="http://www.orcid.org/ns/external-identifier" xmlns:funding="http://www.orcid.org/ns/funding" xmlns:preferences="http://www.orcid.org/ns/preferences" xmlns:address="http://www.orcid.org/ns/address" xmlns:invited-position="http://www.orcid.org/ns/invited-position" xmlns:work="http://www.orcid.org/ns/work" xmlns:history="http://www.orcid.org/ns/history" xmlns:employment="http://www.orcid.org/ns/employment" xmlns:qualification="http://www.orcid.org/ns/qualification" xmlns:service="http://www.orcid.org/ns/service" xmlns:person="http://www.orcid.org/ns/person" xmlns:activities="http://www.orcid.org/ns/activities" xmlns:researcher-url="http://www.orcid.org/ns/researcher-url" xmlns:peer-review="http://www.orcid.org/ns/peer-review" xmlns:bulk="http://www.orcid.org/ns/bulk" xmlns:research-resource="http://www.orcid.org/ns/research-resource">
    <person:person path="/0000-0002-9880-5215/person">
        <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
        <person:name visibility="public" path="0000-0002-9880-5215">
            <common:created-date>2016-04-15T23:25:32.705Z</common:created-date>
            <common:last-modified-date>2016-04-15T23:25:32.705Z</common:last-modified-date>
            <personal-details:given-names>Mihaela</personal-details:given-names>
            <personal-details:family-name>Gheorghe</personal-details:family-name>
        </person:name>
        <other-name:other-names path="/0000-0002-9880-5215/other-names"/>
        <researcher-url:researcher-urls path="/0000-0002-9880-5215/researcher-urls"/>
        <email:emails path="/0000-0002-9880-5215/email"/>
        <address:addresses path="/0000-0002-9880-5215/address"/>
        <keyword:keywords path="/0000-0002-9880-5215/keywords"/>
        <external-identifier:external-identifiers path="/0000-0002-9880-5215/external-identifiers">
            <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
            <external-identifier:external-identifier put-code="674388" visibility="public" path="/0000-0002-9880-5215/external-identifiers/674388" display-index="1">
                <common:created-date>2017-03-24T07:44:50.379Z</common:created-date>
                <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                <common:source>
                    <common:source-client-id>
                        <common:uri>https://orcid.org/client/0000-0002-5982-8983</common:uri>
                        <common:path>0000-0002-5982-8983</common:path>
                        <common:host>orcid.org</common:host>
                    </common:source-client-id>
                    <common:source-name>Scopus - Elsevier</common:source-name>
                    <common:assertion-origin-orcid>
                        <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                        <common:path>0000-0002-9880-5215</common:path>
                        <common:host>orcid.org</common:host>
                    </common:assertion-origin-orcid>
                    <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                </common:source>
                <common:external-id-type>Scopus Author ID</common:external-id-type>
                <common:external-id-value>55177218500</common:external-id-value>
                <common:external-id-url>http://www.scopus.com/inward/authorDetails.url?authorID=55177218500&amp;partnerID=MN8TOARS</common:external-id-url>
                <common:external-id-relationship>self</common:external-id-relationship>
            </external-identifier:external-identifier>
            <external-identifier:external-identifier put-code="674403" visibility="public" path="/0000-0002-9880-5215/external-identifiers/674403" display-index="0">
                <common:created-date>2017-03-24T08:15:43.807Z</common:created-date>
                <common:last-modified-date>2017-03-24T08:15:43.807Z</common:last-modified-date>
                <common:source>
                    <common:source-client-id>
                        <common:uri>https://orcid.org/client/0000-0003-1377-5676</common:uri>
                        <common:path>0000-0003-1377-5676</common:path>
                        <common:host>orcid.org</common:host>
                    </common:source-client-id>
                    <common:source-name>ResearcherID</common:source-name>
                    <common:assertion-origin-orcid>
                        <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                        <common:path>0000-0002-9880-5215</common:path>
                        <common:host>orcid.org</common:host>
                    </common:assertion-origin-orcid>
                    <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                </common:source>
                <common:external-id-type>ResearcherID</common:external-id-type>
                <common:external-id-value>C-7701-2015</common:external-id-value>
                <common:external-id-url>http://www.researcherid.com/rid/C-7701-2015</common:external-id-url>
                <common:external-id-relationship>self</common:external-id-relationship>
            </external-identifier:external-identifier>
        </external-identifier:external-identifiers>
    </person:person>
    <activities:activities-summary path="/0000-0002-9880-5215/activities">
        <common:last-modified-date>2024-01-13T16:16:03.074Z</common:last-modified-date>
        <activities:distinctions path="/0000-0002-9880-5215/distinctions"/>
        <activities:fundings path="/0000-0002-9880-5215/fundings"/>
        <activities:invited-positions path="/0000-0002-9880-5215/invited-positions"/>
        <activities:memberships path="/0000-0002-9880-5215/memberships"/>
        <activities:peer-reviews path="/0000-0002-9880-5215/peer-reviews"/>
        <activities:qualifications path="/0000-0002-9880-5215/qualifications"/>
        <activities:research-resources path="/0000-0002-9880-5215/research-resources"/>
        <activities:services path="/0000-0002-9880-5215/services"/>
        <activities:works path="/0000-0002-9880-5215/works">
            <common:last-modified-date>2022-05-25T15:07:33.009Z</common:last-modified-date>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:25.854Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>doi</common:external-id-type>
                        <common:external-id-value>10.1093/acprof:oso/9780198712350.003.0011</common:external-id-value>
                        <common:external-id-normalized transient="true">10.1093/acprof:oso/9780198712350.003.0011</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313365" path="/0000-0002-9880-5215/work/31313365" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:35:53.579Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:25.854Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-3054-1567</common:uri>
                            <common:path>0000-0002-3054-1567</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Crossref Metadata Search</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Clausal organization and discourse phenomena</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>doi</common:external-id-type>
                            <common:external-id-value>10.1093/acprof:oso/9780198712350.003.0011</common:external-id-value>
                            <common:external-id-normalized transient="true">10.1093/acprof:oso/9780198712350.003.0011</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>other</work:type>
                    <common:publication-date>
                        <common:year>2016</common:year>
                        <common:month>03</common:month>
                        <common:day>03</common:day>
                    </common:publication-date>
                    <work:journal-title>The Syntax of Old Romanian</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:25.823Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>doi</common:external-id-type>
                        <common:external-id-value>10.1093/acprof:oso/9780198712350.003.0009</common:external-id-value>
                        <common:external-id-normalized transient="true">10.1093/acprof:oso/9780198712350.003.0009</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313356" path="/0000-0002-9880-5215/work/31313356" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:34:30.838Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:25.823Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-3054-1567</common:uri>
                            <common:path>0000-0002-3054-1567</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Crossref Metadata Search</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>The complex clause</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>doi</common:external-id-type>
                            <common:external-id-value>10.1093/acprof:oso/9780198712350.003.0009</common:external-id-value>
                            <common:external-id-normalized transient="true">10.1093/acprof:oso/9780198712350.003.0009</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>other</work:type>
                    <common:publication-date>
                        <common:year>2016</common:year>
                        <common:month>03</common:month>
                        <common:day>03</common:day>
                    </common:publication-date>
                    <work:journal-title>The Syntax of Old Romanian</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>other-id</common:external-id-type>
                        <common:external-id-value>01D9EA7786AFE23B85257BAB007CE4A7</common:external-id-value>
                        <common:external-id-normalized transient="true">01d9ea7786afe23b85257bab007ce4a7</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313333" path="/0000-0002-9880-5215/work/31313333" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:31:33.911Z</common:created-date>
                    <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/APP-C2RLHVNFEFIHAT4Q</common:uri>
                            <common:path>APP-C2RLHVNFEFIHAT4Q</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>MLA International Bibliography</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Control Acts in Romanian</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>other-id</common:external-id-type>
                            <common:external-id-value>01D9EA7786AFE23B85257BAB007CE4A7</common:external-id-value>
                            <common:external-id-normalized transient="true">01d9ea7786afe23b85257bab007ce4a7</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2012</common:year>
                    </common:publication-date>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:25.829Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>doi</common:external-id-type>
                        <common:external-id-value>10.1075/ds.17.09ghe</common:external-id-value>
                        <common:external-id-normalized transient="true">10.1075/ds.17.09ghe</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313357" path="/0000-0002-9880-5215/work/31313357" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:34:39.596Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:25.829Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-3054-1567</common:uri>
                            <common:path>0000-0002-3054-1567</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Crossref Metadata Search</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Control acts in Romanian</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>doi</common:external-id-type>
                            <common:external-id-value>10.1075/ds.17.09ghe</common:external-id-value>
                            <common:external-id-normalized transient="true">10.1075/ds.17.09ghe</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>other</work:type>
                    <common:publication-date>
                        <common:year>2012</common:year>
                    </common:publication-date>
                    <work:journal-title>Professional Communication across Languages and Cultures</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:33.003Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>wosuid</common:external-id-type>
                        <common:external-id-value>WOS:000310183800004</common:external-id-value>
                        <common:external-id-normalized transient="true">wos:000310183800004</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31314440" path="/0000-0002-9880-5215/work/31314440" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T08:16:32.312Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:33.003Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0003-1377-5676</common:uri>
                            <common:path>0000-0003-1377-5676</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>ResearcherID</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>INFINITIVAL RELATIVE CLAUSES</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>wosuid</common:external-id-type>
                            <common:external-id-value>WOS:000310183800004</common:external-id-value>
                            <common:external-id-normalized transient="true">wos:000310183800004</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <common:url>http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=ORCID&amp;SrcApp=OrcidOrg&amp;DestLinkType=FullRecord&amp;DestApp=WOS_CPL&amp;KeyUT=WOS:000310183800004&amp;KeyUID=WOS:000310183800004</common:url>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2011</common:year>
                    </common:publication-date>
                    <work:journal-title>Revue Roumaine De Linguistique-Romanian Review of Linguistics</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:27.201Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>eid</common:external-id-type>
                        <common:external-id-value>2-s2.0-84859473285</common:external-id-value>
                        <common:external-id-normalized transient="true">2-s2.0-84859473285</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313608" path="/0000-0002-9880-5215/work/31313608" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:45:02.406Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:27.201Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-5982-8983</common:uri>
                            <common:path>0000-0002-5982-8983</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Scopus - Elsevier</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Infinitival relative clauses</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>eid</common:external-id-type>
                            <common:external-id-value>2-s2.0-84859473285</common:external-id-value>
                            <common:external-id-normalized transient="true">2-s2.0-84859473285</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <common:url>http://www.scopus.com/inward/record.url?eid=2-s2.0-84859473285&amp;partnerID=MN8TOARS</common:url>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2011</common:year>
                    </common:publication-date>
                    <work:journal-title>Revue Roumaine de Linguistique</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:27.207Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>doi</common:external-id-type>
                        <common:external-id-value>10.4067/s0718-09342010000500005</common:external-id-value>
                        <common:external-id-normalized transient="true">10.4067/s0718-09342010000500005</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                    <common:external-id>
                        <common:external-id-type>eid</common:external-id-type>
                        <common:external-id-value>2-s2.0-79251586912</common:external-id-value>
                        <common:external-id-normalized transient="true">2-s2.0-79251586912</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313344" path="/0000-0002-9880-5215/work/31313344" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:34:15.855Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:25.751Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-3054-1567</common:uri>
                            <common:path>0000-0002-3054-1567</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Crossref Metadata Search</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Communicative patterns in Romanian workplace written texts</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>doi</common:external-id-type>
                            <common:external-id-value>10.4067/s0718-09342010000500005</common:external-id-value>
                            <common:external-id-normalized transient="true">10.4067/s0718-09342010000500005</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2010</common:year>
                    </common:publication-date>
                    <work:journal-title>Revista signos</work:journal-title>
                </work:work-summary>
                <work:work-summary put-code="31313609" path="/0000-0002-9880-5215/work/31313609" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:45:02.407Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:27.207Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0002-5982-8983</common:uri>
                            <common:path>0000-0002-5982-8983</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>Scopus - Elsevier</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Communicative patterns in Romanian workplace written texts | Modelos de comunicación en los textos redactados en el ámbito profesional rumano</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>doi</common:external-id-type>
                            <common:external-id-value>10.4067/S0718-09342010000500005</common:external-id-value>
                            <common:external-id-normalized transient="true">10.4067/s0718-09342010000500005</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                        <common:external-id>
                            <common:external-id-type>eid</common:external-id-type>
                            <common:external-id-value>2-s2.0-79251586912</common:external-id-value>
                            <common:external-id-normalized transient="true">2-s2.0-79251586912</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <common:url>http://www.scopus.com/inward/record.url?eid=2-s2.0-79251586912&amp;partnerID=MN8TOARS</common:url>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2010</common:year>
                    </common:publication-date>
                    <work:journal-title>Revista Signos</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2022-05-25T15:07:33.009Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>wosuid</common:external-id-type>
                        <common:external-id-value>WOS:000307752800022</common:external-id-value>
                        <common:external-id-normalized transient="true">wos:000307752800022</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31314441" path="/0000-0002-9880-5215/work/31314441" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T08:16:32.313Z</common:created-date>
                    <common:last-modified-date>2022-05-25T15:07:33.009Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/0000-0003-1377-5676</common:uri>
                            <common:path>0000-0003-1377-5676</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>ResearcherID</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>ADVANTAGES OF USING THE CONCEPT OF &amp;quot;TRACE&amp;quot; IN SYNTACTIC DESCRIPTION</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>wosuid</common:external-id-type>
                            <common:external-id-value>WOS:000307752800022</common:external-id-value>
                            <common:external-id-normalized transient="true">wos:000307752800022</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <common:url>http://gateway.webofknowledge.com/gateway/Gateway.cgi?GWVersion=2&amp;SrcAuth=ORCID&amp;SrcApp=OrcidOrg&amp;DestLinkType=FullRecord&amp;DestApp=WOS_CPL&amp;KeyUT=WOS:000307752800022&amp;KeyUID=WOS:000307752800022</common:url>
                    <work:type>book</work:type>
                    <common:publication-date>
                        <common:year>2005</common:year>
                    </common:publication-date>
                    <work:journal-title>European Integration: between Tradition and Modernity, Vol 1</work:journal-title>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>other-id</common:external-id-type>
                        <common:external-id-value>852573C90056F867852571ED00753C5B</common:external-id-value>
                        <common:external-id-normalized transient="true">852573c90056f867852571ed00753c5b</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313335" path="/0000-0002-9880-5215/work/31313335" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:31:40.320Z</common:created-date>
                    <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/APP-C2RLHVNFEFIHAT4Q</common:uri>
                            <common:path>APP-C2RLHVNFEFIHAT4Q</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>MLA International Bibliography</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Gramatica limbii române, II: Enunțul</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>other-id</common:external-id-type>
                            <common:external-id-value>852573C90056F867852571ED00753C5B</common:external-id-value>
                            <common:external-id-normalized transient="true">852573c90056f867852571ed00753c5b</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>book</work:type>
                    <common:publication-date>
                        <common:year>2005</common:year>
                    </common:publication-date>
                </work:work-summary>
            </activities:group>
            <activities:group>
                <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                <common:external-ids>
                    <common:external-id>
                        <common:external-id-type>other-id</common:external-id-type>
                        <common:external-id-value>A1771B201CAA58B3852574F8007E0F87</common:external-id-value>
                        <common:external-id-normalized transient="true">a1771b201caa58b3852574f8007e0f87</common:external-id-normalized>
                        <common:external-id-relationship>self</common:external-id-relationship>
                    </common:external-id>
                </common:external-ids>
                <work:work-summary put-code="31313336" path="/0000-0002-9880-5215/work/31313336" visibility="public" display-index="0">
                    <common:created-date>2017-03-24T07:31:44.360Z</common:created-date>
                    <common:last-modified-date>2017-03-24T08:15:43.826Z</common:last-modified-date>
                    <common:source>
                        <common:source-client-id>
                            <common:uri>https://orcid.org/client/APP-C2RLHVNFEFIHAT4Q</common:uri>
                            <common:path>APP-C2RLHVNFEFIHAT4Q</common:path>
                            <common:host>orcid.org</common:host>
                        </common:source-client-id>
                        <common:source-name>MLA International Bibliography</common:source-name>
                        <common:assertion-origin-orcid>
                            <common:uri>https://orcid.org/0000-0002-9880-5215</common:uri>
                            <common:path>0000-0002-9880-5215</common:path>
                            <common:host>orcid.org</common:host>
                        </common:assertion-origin-orcid>
                        <common:assertion-origin-name>Mihaela Gheorghe</common:assertion-origin-name>
                    </common:source>
                    <work:title>
                        <common:title>Un caz particular de opacizare a joncțiunii relative</common:title>
                    </work:title>
                    <common:external-ids>
                        <common:external-id>
                            <common:external-id-type>other-id</common:external-id-type>
                            <common:external-id-value>A1771B201CAA58B3852574F8007E0F87</common:external-id-value>
                            <common:external-id-normalized transient="true">a1771b201caa58b3852574f8007e0f87</common:external-id-normalized>
                            <common:external-id-relationship>self</common:external-id-relationship>
                        </common:external-id>
                    </common:external-ids>
                    <work:type>journal-article</work:type>
                    <common:publication-date>
                        <common:year>2003</common:year>
                    </common:publication-date>
                </work:work-summary>
            </activities:group>
        </activities:works>
    </activities:activities-summary>
</record:record>
`;